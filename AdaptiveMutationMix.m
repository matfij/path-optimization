function [path, cost] = AdaptiveMutationMix(debugging, map, start, goal, generationSize, generationNumber, maxGenotypeLength)
    
    % problem reprezentation
    mapWidth = length(map(1, :));
    mapHigth = length(map(:, 1));
    mapFunction = map;
    goalX = goal(1);
    goalY = goal(2);
    newGenX = start(1);
    newGenY = start(2);

    % solution vessel
    bestPath(1, :) = [0 0];
    bestFitness = inf;
    
    generationMembers(1) = Trooper;
    sortedGeneration(1) = Trooper;
    
    % algorithm parameters
    firstGenerationMultiplayer = 2;
    minGenotypeLength = 5;
    
    eliteMembers = 2;
    nBest = floor(0.5 * generationSize);
    
    baseMutationStep = 100;
    C1 = floor(generationNumber / 2);
    C2 = 4;

    macroMutationThreshold = floor(0.6 * generationSize);
    macroMutatedGens = 0.2;
    macroStep = 50;
    
    % states, counters
    currentMember = 1;
    currentGeneration = 1;
    mutationStep = baseMutationStep;
    legalPoint = 0;
    progress = 1;
    
    % first generation
    while(currentMember < firstGenerationMultiplayer * generationSize)
        
        tempGenotypeLength = randi(floor(maxGenotypeLength)) + minGenotypeLength;
        tempGenotype = randi(mapWidth, tempGenotypeLength, 2);
        tempGenotype(1, 1) = start(1);
        tempGenotype(1, 2) = start(2);
        tempGenotype(tempGenotypeLength, 1) = goalX;
        tempGenotype(tempGenotypeLength, 2) = goalY;

        generationMembers(currentMember).gLength = tempGenotypeLength;
        generationMembers(currentMember).genotype = tempGenotype;
        generationMembers(currentMember).phenotype = translatePhenotype(generationMembers(currentMember));
        generationMembers(currentMember).fitness = calcFitnessAdaptive(generationMembers(currentMember), mapFunction, progress);
 
        currentMember = currentMember + 1;
    end
    
    
    % uitdoving ... 
    
    
    % every new generation
    while(currentGeneration < generationNumber)
        
        % evaluation & selection
        [out, idx] = sort([generationMembers.fitness], 'ascend');
        for currentMember = 1 : generationSize
            
            sortedGeneration(currentMember).fitness = out(currentMember);
            sortedGeneration(currentMember).gLength = generationMembers(idx(currentMember)).gLength;
            sortedGeneration(currentMember).genotype = generationMembers(idx(currentMember)).genotype;
        end
        
        
        % successie - elites
        for currentMember = 1 : eliteMembers
            
            generationMembers(currentMember).gLength = sortedGeneration(currentMember).gLength;
            generationMembers(currentMember).genotype = sortedGeneration(currentMember).genotype;
            
            generationMembers(currentMember).phenotype = translatePhenotype(generationMembers(currentMember));
            generationMembers(currentMember).fitness = calcFitnessAdaptive(generationMembers(currentMember), mapFunction, progress);
        end

        
        % successie - others
        for currentMember = eliteMembers : generationSize
            
            parentMember = randi(nBest);
            reproductionMethod = randi(generationSize);
   
            % macro mutation  
            if(reproductionMethod < macroMutationThreshold)
                
                generationMembers(currentMember).gLength = sortedGeneration(parentMember).gLength;
                generationMembers(currentMember).genotype = sortedGeneration(parentMember).genotype;
                
                alterGensLimit = randi(floor(macroMutatedGens * sortedGeneration(parentMember).gLength));
                
                changedPoints = randi(alterGensLimit);

                for i = 1 : changedPoints
                    
                    changedPoint = randi(generationMembers(currentMember).gLength - 1) + 1;
                    
                        while(legalPoint == 0)
                            
                            newGenX = sortedGeneration(parentMember).genotype(changedPoint, 1) + ...
                                (randi(macroStep) - floor(0.5*macroStep));
                            newGenY = sortedGeneration(parentMember).genotype(changedPoint, 2) + ...
                                (randi(macroStep) - floor(0.5*macroStep));

                            if(newGenX < mapWidth && newGenX > 0 && newGenY < mapHigth && newGenY > 0)
                                legalPoint = 1;
                            end
                        end
                        legalPoint = 0;
                       
                        generationMembers(currentMember).genotype(changedPoint, :) = [newGenX newGenY];
                end
                
                
            % regular mutation  
            else
                
                for subPoint = 2 : sortedGeneration(parentMember).gLength
                    
                    while(legalPoint == 0)

                        newGenX = sortedGeneration(parentMember).genotype(subPoint, 1) + ...
                            (randi(mutationStep) - floor(0.5*mutationStep));
                        newGenY = sortedGeneration(parentMember).genotype(subPoint, 2) + ...
                            (randi(mutationStep) - floor(0.5*mutationStep));

                        if(newGenX < mapWidth && newGenX > 0 && newGenY < mapHigth && newGenY > 0)
                            legalPoint = 1;
                        end
                    end
                    legalPoint = 0;  

                    generationMembers(currentMember).genotype(subPoint, :) = [newGenX newGenY];
                end

            end
            
            generationMembers(currentMember).genotype(generationMembers(currentMember).gLength, :) = [goalX goalY];
            
            generationMembers(currentMember).phenotype = translatePhenotype(generationMembers(currentMember));
            generationMembers(currentMember).fitness = calcFitnessAdaptive(generationMembers(currentMember), mapFunction, progress);   

        end  % single generation successie
        
        
        % save results
        bestFitness = generationMembers(1).fitness;
        bestPath = generationMembers(1).phenotype;

        if(debugging == 1)
            
            elite = generationMembers(1);
            average = generationMembers(floor(generationSize/2));
            worst = generationMembers(generationSize);

            Plotter.plotMap(Plotter, map, start, goal, elite, average, worst, currentGeneration);

            eliteFitness(currentGeneration) = elite.fitness;
            averageFitness(currentGeneration) = average.fitness;
            worstFitness(currentGeneration) = worst.fitness;
            
            Plotter.plotConvergationCurve(Plotter, eliteFitness, averageFitness, worstFitness);

            elite.gLength
        end

        
        % update algorithm parameters
        mutationStep = 2 + floor(baseMutationStep * (1/(1+exp((currentGeneration-C1)/C2))));
        
        currentMember = 1;
        currentGeneration = currentGeneration + 1;
        progress = currentGeneration / generationNumber;
    
    end % generations loop

    
% finally save results
cost = bestFitness;
path = bestPath;

