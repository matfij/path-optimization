close all
clear
clc

%loading a map
map = MapPreparation('Maps/Map1.png');

%defining path endpoints
start = [1, 50];
goal = [251, 355];


%calling first algorithm: A_Fijak_mk1
fprintf('\nTesting A_Fijak_mk1 algorithm\n');
pathFijak1 = A_Fijak_mk1(map, start, goal);


%plot results
TimeOfTravel(1,:) = PathEvaluation(map, start, goal, pathFijak1,'-r');


%show path cost
TimeOfTravel