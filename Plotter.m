
classdef Plotter
   
    properties
       
        eliteColor = 'c';
        averageColor = 'g';
        worstColor = 'r';
        
        pauseTime = 0;
    end
    
    
   methods(Static)
       
      function plotMap(self, map, start, goal, elite, average, worst, generation)
         
            figure(1)
            
            imshow(map);
            hold on
            plot(start(1), start(2), 'ro')
            hold on
            plot(goal(1), goal(2), 'go')

%             hold on
%             plot(worst.phenotype(:, 1), worst.phenotype(:, 2), self.worstColor, 'LineWidth', 2)
%             
%             hold on
%             plot(average.phenotype(:, 1), average(1).phenotype(:, 2), self.averageColor, 'LineWidth', 2)
%             
            hold on
            plot(elite.phenotype(:, 1), elite.phenotype(:, 2), self.eliteColor, 'LineWidth', 2)

            pause(self.pauseTime)
      end
      
      
      function plotConvergationCurve(self, elite, average, worst)
          
        figure(2)

        plot(worst, self.worstColor)
        hold on
        plot(average, self.averageColor)
        hold on
        plot(elite, self.eliteColor)
        
        xlabel("generations")
        ylabel("path cost")
        legend("worst member", "average member", "elite")
      end
      
      
      function plotPath(self, path)
          
          hold on
          plot(path(:, 1),path(:, 2), self.eliteColor, 'LineWidth', 2)
      end
      
    end
end