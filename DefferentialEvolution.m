function [path, cost] = DefferentialEvolution(debugging, map, start, goal, generationSize, generationNumber, genotypeLength)
    
    % problem reprezentation
    mapWidth = length(map(1, :));
    mapHeight = length(map(:, 1));
    mapSize = min(mapWidth, mapHeight);
    mapFunction = map;
    goalX = goal(1);
    goalY = goal(2);
    startX = start(1);
    startY = start(2);

    % solution vessel
    bestPath(1, :) = [0 0];
    bestFitness = inf;
    
    generationMembers(1) = Trooper;
    sortedGeneration(1) = Trooper;
    
    % algorithm parameters
    firstGenerationMultiplayer = 2;
    
    baseMutationStep = 100;
    mutationStep = baseMutationStep;
    C1 = floor(generationNumber / 2);
    C2 = 4;

    % states, counters
    currentMember = 1;
    currentGeneration = 1;

    
    % first generation
    while(currentMember < firstGenerationMultiplayer * generationSize)
        
        tempGenotype = randi(min(mapWidth, mapHeight), genotypeLength, 2);
        tempGenotype(1, 1) = startX;
        tempGenotype(1, 2) = startY;
        tempGenotype(genotypeLength, 1) = goalX;
        tempGenotype(genotypeLength, 2) = goalY;

        generationMembers(currentMember).gLength = genotypeLength;
        generationMembers(currentMember).genotype = tempGenotype;
        generationMembers(currentMember).phenotype = translatePhenotype(generationMembers(currentMember));
        generationMembers(currentMember).fitness = calcFitness(generationMembers(currentMember), mapFunction);
 
        currentMember = currentMember + 1;
    end
    
    
    % uitdoving ... 
    
    
    % every new generation
    while(currentGeneration < generationNumber + 1)
    
        % sorting population
        [out, idx] = sort([generationMembers.fitness], 'ascend');
        for currentMember = 1 : generationSize
            
            sortedGeneration(currentMember).fitness = out(currentMember);
            sortedGeneration(currentMember).genotype = generationMembers(idx(currentMember)).genotype;
        end
        
        % successie - all population
        for currentMember = 1 : generationSize
            
            mainParentGenotype = sortedGeneration(currentMember).genotype;
            
            supportParentA = sortedGeneration(randi(generationSize)).genotype;
            supportParentB = sortedGeneration(randi(generationSize)).genotype;
            supportParentC = sortedGeneration(randi(generationSize)).genotype;
            
            supportParentGenotype = floor(1/3 * (supportParentA + supportParentB + supportParentC));
            
            % crossover
            for subPoint = 2 : genotypeLength - 1
                
                parent = randi(6);
                
                if(parent > 3)
                    
                    generationMembers(currentMember).genotype(subPoint, :) = max(min(mainParentGenotype(subPoint, :) + ...
                        [randi(mutationStep)-floor(0.5*mutationStep) randi(mutationStep)-floor(0.5*mutationStep)], ...
                        [mapSize mapSize]), [1 1]);
                else
                    
                    generationMembers(currentMember).genotype(subPoint, :) = max(min(supportParentGenotype(subPoint, :) + ...
                        [randi(mutationStep)-floor(0.5*mutationStep) randi(mutationStep)-floor(0.5*mutationStep)], ...
                        [mapSize mapSize]), [1 1]);
                end
                
            end

            generationMembers(currentMember).phenotype = translatePhenotype(generationMembers(currentMember));
            generationMembers(currentMember).fitness = calcFitness(generationMembers(currentMember), mapFunction);
            
            % check if child is better than parent
            if(sortedGeneration(currentMember).fitness < generationMembers(currentMember).fitness)
                
                generationMembers(currentMember).genotype = sortedGeneration(currentMember).genotype;
                generationMembers(currentMember).phenotype = translatePhenotype(generationMembers(currentMember));
                generationMembers(currentMember).fitness = calcFitness(generationMembers(currentMember), mapFunction);
            end
        end


        % save results
        bestFitness = generationMembers(1).fitness;
        bestPath = generationMembers(1).phenotype;

        if(debugging == 1)
            
            elite = generationMembers(1);
            average = generationMembers(floor(generationSize/2));
            worst = generationMembers(generationSize);
            
            Plotter.plotMap(Plotter, map, start, goal, elite, average, worst, currentGeneration);
            
            eliteFitness(currentGeneration) = elite.fitness;
            averageFitness(currentGeneration) = average.fitness;
            worstFitness(currentGeneration) = worst.fitness;
            
            Plotter.plotConvergationCurve(Plotter, eliteFitness, averageFitness, worstFitness);
        end


        % update algorithm parameters
        mutationStep = 2 + floor(baseMutationStep * (1/(1+exp((currentGeneration-C1)/C2))));

        currentGeneration = currentGeneration + 1;   
    
    end % generations loop

    
% finally save results
cost = bestFitness;
path = bestPath;

