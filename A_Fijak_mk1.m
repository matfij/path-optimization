function [path] = A_Fijak_mk1(map, start, goal)

%problem reprezentation
mapWidth = length(map(1, :));
mapHigth = length(map(:, 1));

goalX = goal(1);
goalY = goal(2);

%solution vessel
path(1, :) = [start(1), start(2)];

%program counter
cnt = 1;

%program state
finished = 0;
finishedX = 0;
finishedY = 0;

%algorithm parameters
stepSize = 1;

while (finished == 0)
    
    %approaching X direction
    if(finishedX == 0)
         
        if(path(cnt, 1) < goalX)
            path(cnt+1, 1) = path(cnt, 1) + stepSize;
            path(cnt+1, 2) = path(cnt, 2);
        end

        if(path(cnt, 1) > goalX)
            path(cnt+1, 1) = path(cnt, 1) - stepSize;
            path(cnt+1, 2) = path(cnt, 2);
        end
        
        if(path(cnt, 1) == goalX)
            finishedX = 1;
        end
        
    end
   
    
    %approaching Y direction
    if(finishedY == 0 && finishedX ~= 0)
        
        if(path(cnt, 2) < goalY)
            path(cnt+1, 2) = path(cnt, 2) + stepSize;
            path(cnt+1, 1) = path(cnt, 1);
        end

        if(path(cnt, 2) > goalY)
            path(cnt+1, 2) = path(cnt, 2) - stepSize;
            path(cnt+1, 1) = path(cnt, 1);
        end
        
         if(path(cnt, 2) == goalY)
            finishedY = 1;
         end
         
    end
    
    %checking if goal is reached
    if(finishedX == 1 && finishedY == 1)
        finished = 1;
    end
    
    cnt = cnt + 1;
    
end



end




