
classdef Trooper
    
    properties

        genotype
        gLength
        phenotype
        fitness
    end


    methods

        % geno/pheno transition
        function phenotype = translatePhenotype(trooper)
            
            trooperPath(1, :) = [trooper.genotype(1, 1), trooper.genotype(1, 2)];
            length(trooper.genotype);
            k = 1;

            for point = 2 : trooper.gLength

                tempGoalX = trooper.genotype(point, 1);
                tempGoalY = trooper.genotype(point, 2);
                pointFinished = 0;
                
                while(pointFinished == 0)
                    if(abs(tempGoalX - trooperPath(k,1)) > abs(tempGoalY - trooperPath(k,2)))
                        trooperPath(k+1,1) = trooperPath(k,1) + sign(tempGoalX - trooperPath(k,1));
                        trooperPath(k+1,2) = trooperPath(k,2);
                    else
                        trooperPath(k+1,2) = trooperPath(k,2) + sign(tempGoalY - trooperPath(k,2));
                        trooperPath(k+1,1) = trooperPath(k,1);
                    end
                    k = k + 1;

                    if(trooperPath(k,1) == tempGoalX && trooperPath(k,2) == tempGoalY)
                        pointFinished = 1;
                    end
                end
            end
            phenotype = trooperPath;
        end

        
        function fitness = calcFitnessAdaptive(trooper, map, progress)
            
            fitness = 0;

            for k = 1 : length(trooper.phenotype)
                
                fitness = fitness + cast( map(trooper.phenotype(k, 2), trooper.phenotype(k, 1)), 'double') + 1;    
            end

            % prominence
%             progressWeight = 0.5;
%             
%             fitness = fitness / (1 + trooper.gLength / (progress * trooper.gLength));
        end
        
        
        function fitness = calcFitness(trooper, map)
            
            fitness = 0;

            for k = 1 : length(trooper.phenotype)
                
                fitness = fitness + cast( map(trooper.phenotype(k, 2), trooper.phenotype(k, 1)), 'double') + 1;
            end
            
        end

        
    end
end