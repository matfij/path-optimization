close all
clear
clc

% problem representation
rawMap = imread('Maps/Map0.png');
map = rgb2gray(rawMap);

start = [15, 15];
goal = [385, 385];

% problem visualization
imshow(rawMap);
hold on
plot(start(1), start(2), 'ro')
hold on
plot(goal(1), goal(2), 'go')


% testing algorithms only one at once reccomended

% debugging, map, start, goal, generationSize, generationNumber, maxGenotypeLength
% [path, cost] = AdaptiveMutationMix(0, map, start, goal, 50, 19, 15);

% debugging, map, start, goal, generationSize, generationNumber, genotypeLength
 [path, cost] = DefferentialEvolution(0, map, start, goal, 220, 49, 10);


% showing results
Plotter.plotPath(Plotter, path)
cost

